import declare::*;

module Computer(
		input 	     CLOCK_50,
		input [3:0]  SW,
		input [1:0]  KEY,
		output [7:0] LED
		);

   // Locals
   logic 		     dec_to_hal;
   
   // Modules
   HAL hal(SW, KEY, LED, dec_to_hal);
   
   typedef enum logic [2:0]
		{
		 cmd_none,
		 cmd_LED_on,
		 cmd_LED_off
		 } cmd_t;

   cmd_t cmd = cmd_none;

   always_ff @ (posedge CLOCK_50) begin

      case (cmd)
	cmd_LED_on:
	  begin
	     
	  end
	
	cmd_LED_off:
	  begin
	     
	  end  

      endcase // case (cmd)
      
   end

endmodule // Computer
