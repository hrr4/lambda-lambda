// `define HAL_ENTRY(NAME, MAX_BITS) \

package declare;

   //localparam HW_SZ = 6;
   
   typedef enum logic [5:0]
		{
		 SW0,
		 SW1,
		 SW2,
		 KEY0,
		 KEY1,
		 LED0,
		 LED1,
		 LED2,
		 LED3,
		 LED4,
		 LED5,
		 LED6,
		 LED7,
		 HW_SZ
		 } hw_t;


endpackage: declare
